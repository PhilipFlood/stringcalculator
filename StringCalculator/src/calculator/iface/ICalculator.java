/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.iface;

/**
 * @author eflophi Simple interface for a String calculator
 *
 */
public interface ICalculator {
    /**
     * calculates integer addition when given as a string
     *
     * @return the result of given addition sum
     * @param sum
     *            an integer sum given in String form
     */
    String add(String sum);

    /**
     * calculates integer subtraction when given as a string
     *
     * @return the result of given subtraction sum
     * @param sum
     *            an integer sum given in String form
     */
    String subtract(String sum);

    /**
     * calculates integer division when given as a string
     *
     * @return the result of given division sum
     * @param sum
     *            an integer sum given in String form
     */
    String divide(String sum);

    /**
     * calculates integer multiplication when given as a string
     *
     * @return the result of given multiplication sum
     * @param sum
     *            an integer sum given in String form
     */
    String multiply(String sum);
}
