/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.driver;

import calculator.impl.CalculatorMenu;

/**
 * @author eflophi Class used to launch String calculator
 */
public class Main {
    static CalculatorMenu menu = new CalculatorMenu();

    /** launches the String calculator program */
    public static void main(final String[] args) {
        menu.display();
    }
}
