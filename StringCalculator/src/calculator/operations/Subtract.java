/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.operations;

/**
 * @author eflophi Class used to calculate integer subtraction when given as a string
 *
 */
public class Subtract implements Operation {
    /**
     * calculates a subtraction sum given in string format
     *
     * @returns the answer of the subtraction sum
     * @param sum
     *            sum given to the method (eg. "1-2")
     */
    @Override
    public String execute(final String sum) {
        return Integer.toString((int) stringCalculate(sum));
    }

    /** converts the two strings to int so an answer can be gotten then returned */
    private double stringCalculate(final String sum) {
        final String holder = sum.replace("-", " -");
        final String[] numbers = holder.split(" ");

        if ("".equals(numbers[0])) {
            return Double.parseDouble(numbers[1]) + Double.parseDouble(numbers[2]);
        }
        return Double.parseDouble(numbers[0]) + Double.parseDouble(numbers[1]);
    }
}
