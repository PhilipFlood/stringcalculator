/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.operations;

/**
 * @author eflophi
 *
 */
public interface Operation {
    String execute(String sum);
}
