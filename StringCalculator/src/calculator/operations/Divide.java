/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.operations;

/**
 * @author eflophi Class used to calculate integer division when given as a string
 *
 */
public class Divide implements Operation {
    /**
     * calculates a division sum given in string format
     *
     * @returns the answer of the division sum
     * @param sum
     *            sum given to the method (eg. "2/2")
     */
    @Override
    public String execute(final String sum) {
        return Integer.toString((int) stringCalculate(sum));
    }

    /** converts the two strings to int so an answer can be gotten then returned */
    private double stringCalculate(final String sum) {
        final String[] numbers = sum.split("\\/");
        return Double.parseDouble(numbers[0]) / Double.parseDouble(numbers[1]);
    }
}
