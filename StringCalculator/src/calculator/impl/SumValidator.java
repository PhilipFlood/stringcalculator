/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author eflophi Class to validate and check that a mathematical sum is valid when given as a string
 *
 */
public class SumValidator {
    /**
     * ensures that a given sum is a valid sum by checking it has equal brackets, no duplicated operators side by side and valid characters
     *
     * @returns true if valid
     * @param userInput
     *            The sum the user has given
     **/
    public boolean validate(final String userInput) {
        if (checkForValidCharacters(userInput) && checkBracketAmount(userInput) && checkForDoubleOperator(userInput)) {
            return true;
        }
        return false;
    }

    /** uses a regex to ensure a given string is comprised only of numbers & 4 operators: '+','-','*','/' */
    private boolean checkForValidCharacters(final String value) {
        final String validSum = "^[0-9,*,/,\\-,+,(,)]+$";
        final Pattern pattern = Pattern.compile(validSum);
        final Matcher matcher = pattern.matcher(value);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("value contains illegal characters");
        }
        return true;
    }

    /** checks that there are an equal amount of parentheses in a given string sum */
    private boolean checkBracketAmount(String value) {

        if (value.lastIndexOf(')') > value.lastIndexOf('(')) {
            String innerSum = (String) value.subSequence(value.lastIndexOf('(') + 1, value.lastIndexOf(')'));
            if (innerSum.contains(")")) {
                innerSum = innerSum.substring(0, innerSum.indexOf(')'));
            }
            value = value.replace("(" + innerSum + ")", "");
            return checkBracketAmount(value);
        }
        if (!value.contains("(") && !value.contains(")")) {
            return true;
        }
        return false;
    }

    /** checks that two operators are not side by side */
    private boolean checkForDoubleOperator(final String value) {
        if (value.contains("**") || value.contains("*/") || value.contains("/*") || value.contains("//")) {
            throw new IllegalArgumentException("value contains illegal duplicated operators");
        }
        return true;
    }
}
