/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

import calculator.iface.ICalculator;
import calculator.operations.Add;
import calculator.operations.Divide;
import calculator.operations.Multiply;
import calculator.operations.Subtract;

/**
 * @author eflophi Calculator that takes in and returns sums given from a string format
 *
 */
public class StringCalculator implements ICalculator {

    Add add = new Add();
    Subtract subtract = new Subtract();
    Multiply multiply = new Multiply();
    Divide divide = new Divide();

    @Override
    public String add(final String sum) {
        return add.execute(sum);
    }

    @Override
    public String subtract(final String sum) {
        return subtract.execute(sum);
    }

    @Override
    public String divide(final String sum) {
        return divide.execute(sum);
    }

    @Override
    public String multiply(final String sum) {
        return multiply.execute(sum);
    }
}
