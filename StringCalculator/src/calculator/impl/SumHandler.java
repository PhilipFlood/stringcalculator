/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

import java.util.ArrayList;

/**
 * @author eflophi Class that handles complex String sums and uses a calculator to compute what it handles
 *
 */
public class SumHandler {

    StringCalculator calc = new StringCalculator();

    /**
     * calculates arbitrarily complex String expressions using addition, subtraction, multiplication and division operators (with normal precedence
     * rules)
     *
     * @param sum
     *            - The given calculation
     * @return The answer in String form, will be a single number
     */
    public String solveSum(final String sum) {
        String result = sum;
        result = calculateBracketSums(result);
        ArrayList<String> allSums = findAllSumsInString(result);
        result = calculateMultiplyDivide(result, allSums, "*");
        allSums = findAllSumsInString(result);
        result = calculateMultiplyDivide(result, allSums, "/");
        allSums = findAllSumsInString(result);
        result = calculateAddSubtract(result, allSums);
        return result;
    }

    /** finds and completes any sums found in brackets and returns string with the answers */
    private String calculateBracketSums(String largeSum) {
        while (largeSum.contains("(")) {
            String innerSum = (String) largeSum.subSequence(largeSum.lastIndexOf('(') + 1, largeSum.lastIndexOf(')'));
            if (innerSum.contains(")")) {
                innerSum = innerSum.substring(0, innerSum.indexOf(')'));
            }
            largeSum = largeSum.replace("(" + innerSum + ")", solveSum(innerSum));
        }
        return largeSum;
    }

    /**
     * loops through all internal sums looking for sums of the given operation and returns the big sum with those inner sums replaced by their answer
     */
    private String calculateMultiplyDivide(String largeSum, ArrayList<String> allSums, final String operator) {
        while (largeSum.contains(operator)) {
            for (int i = 0; i < allSums.size(); i++) {
                if (allSums.get(i).contains(operator)) {
                    largeSum = largeSum.replace(allSums.get(i), getOperation(allSums.get(i), operator));
                }
            }
            allSums = findAllSumsInString(largeSum);
        }
        return largeSum;
    }

    /** completes all additions and subtractions left to right and returns the result */
    private String calculateAddSubtract(String largeSum, ArrayList<String> allSums) {
        while (largeSum.contains("+") || largeSum.lastIndexOf("-") > 0) {

            allSums = findAllSumsInString(largeSum);
            if (allSums.get(0).contains("+")) {
                largeSum = largeSum.replace(allSums.get(0), calc.add(allSums.get(0)));
            }
            else if (allSums.get(0).contains("-")) {
                largeSum = largeSum.replace(allSums.get(0), calc.subtract(allSums.get(0)));
            }
        }
        return largeSum;
    }

    /** returns the calculation of given operator */
    private String getOperation(final String sum, final String operator) {
        if ("*".equals(operator)) {
            return calc.multiply(sum);
        }
        return calc.divide(sum);
    }

    /** loops through the sum string, finds all inner sums and returns an arraylist containing every sum. */
    private ArrayList<String> findAllSumsInString(final String largeSum) {
        final char[] sumCharacters = largeSum.toCharArray();
        final ArrayList<String> allSums = new ArrayList<>();
        int subSumStart = 0;
        int nextOperator = 0;
        int numLength = 0;

        for (int i = 0; i < sumCharacters.length - 1; i++) {
            if (!Character.isDigit(sumCharacters[i + 1]) && nextOperator == 0 && sumCharacters[i + 1] != '.') {
                nextOperator = i + 1;
                numLength = 0;
            }
            else if (!Character.isDigit(sumCharacters[i + 1]) && numLength != 0 && sumCharacters[i + 1] != '.') {
                allSums.add(getSubSum(largeSum, sumCharacters[subSumStart], subSumStart, i + 1));
                subSumStart = nextOperator;
                i = nextOperator;
                nextOperator = 0;
                numLength = 0;
            }
            else if (i == sumCharacters.length - 2) {
                allSums.add(getSubSum(largeSum, sumCharacters[subSumStart], subSumStart, sumCharacters.length));
            }
            else {
                numLength++;
            }
        }
        return allSums;
    }

    /** returns an inner sum from specified sum at spefied indexes. Checks if there is a negative number and adds the - sign if so */
    private String getSubSum(final String fullSum, final char character, final int subSumStart, final int subSumEnd) {
        if (character == '-' || Character.isDigit(character)) {
            return fullSum.substring(subSumStart, subSumEnd);
        }
        return fullSum.substring(subSumStart + 1, subSumEnd);
    }
}