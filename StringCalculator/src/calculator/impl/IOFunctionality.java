/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

import java.util.Scanner;

/**
 * @author eflophi Class to perform input and output functionality
 */
public class IOFunctionality {

    private final Scanner in = new Scanner(System.in);

    /**
     * returns input user types to console
     *
     * @return user input string
     */
    public String readLine() {
        return in.nextLine().toUpperCase();
    }

    /**
     * displays an opening menu
     *
     * @param text
     *            to be output to console
     */
    public void out(final String text) {
        System.out.println(text);
    }
}