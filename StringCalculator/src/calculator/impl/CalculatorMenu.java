/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

/**
 * @author eflophi class to show a menu to the user for the string calculator
 *
 */
public class CalculatorMenu {
    private final SumHandler converter = new SumHandler();
    private final IOFunctionality io = new IOFunctionality();
    private final SumValidator validator = new SumValidator();

    /** Displays a menu to the user for the String calculator */
    public void display() {
        String sum = "";

        while (!"0".equals(sum)) {
            io.out("Enter a sum: (type '0' to leave)");
            sum = io.readLine();

            try {
                validator.validate(sum);
                io.out(converter.solveSum(sum));
            }
            catch (final IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }
}
