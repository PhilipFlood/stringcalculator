/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.StringCalculator;

/**
 * @author eflophi
 *
 */
public class DivideTest {

    StringCalculator calc = new StringCalculator();

    @Test
    public void testDivide() {
        assertEquals("5", calc.divide("10/2"));
        assertEquals("10", calc.divide("40/4"));
        assertEquals("-5", calc.divide("-10/2"));
        assertEquals("20", calc.divide("-100/-5"));
        assertEquals("127", calc.divide("71628/564"));
    }

    @Test
    public void testDivideWithFractions() {
        assertEquals("3", calc.divide("13/4"));
    }
}
