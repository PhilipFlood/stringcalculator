/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.StringCalculator;

/**
 * @author eflophi
 *
 */
public class AddTest {

    StringCalculator calc = new StringCalculator();

    @Test
    public void testAddition() {
        assertEquals("10", calc.add("5+5"));
        assertEquals("103", calc.add("73+30"));
        assertEquals("1612", calc.add("1012+600"));
        assertEquals("20", calc.add("-10+30"));
    }
}
