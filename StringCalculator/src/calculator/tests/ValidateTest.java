/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.tests;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import calculator.impl.SumValidator;

/**
 * @author eflophi
 *
 */
public class ValidateTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    SumValidator valid = new SumValidator();

    @Test
    public void testValidation() {
        assertEquals(true, valid.validate("17*2/125"));

        assertEquals(false, valid.validate("(1+2)+)1+2(")); // SHOULD BE FALSE

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("value contains illegal characters");
        assertEquals(false, valid.validate("sdfghjk"));
        assertEquals(false, valid.validate("8*2+1(sd)"));
        thrown.expectMessage("value doesn't contain an equal number of opening and closing parentheses");
        assertEquals(false, valid.validate("1*2+(5+2"));
        thrown.expectMessage("value contains closing bracket before opening bracket");
        assertEquals(false, valid.validate(")5("));
    }

    @Test
    public void testBiggerSums() {
        assertEquals(true, valid.validate("6-2+3-2*4-5"));
        assertEquals(true, valid.validate("(48/2)*(9+3)-400+3/-2"));
        assertEquals(true, valid.validate("(((-2+3)-4)*5)+((-5+2)*100)-400"));
    }

    @Test
    public void testValidationWithDuplicates() {
        assertEquals(true, valid.validate("5--1"));
        assertEquals(true, valid.validate("5++1"));

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("value contains illegal duplicated operators");
        assertEquals(false, valid.validate("5**1"));
        assertEquals(false, valid.validate("5//1"));
    }
}
