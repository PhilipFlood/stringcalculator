/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.SumHandler;

/**
 * @author eflophi
 *
 */
public class CalculatorTest {

    SumHandler calc = new SumHandler();

    @Test
    public void testCalculator() {
        assertEquals("4", calc.solveSum("2+2"));
        assertEquals("792656", calc.solveSum("856*926"));
        assertEquals("2", calc.solveSum("4-2"));
        assertEquals("2", calc.solveSum("10/5"));
        assertEquals("-14", calc.solveSum("-3+4-10*2+5"));
    }

    @Test
    public void testCalculatorWithComplexSums() {
        assertEquals("20", calc.solveSum("(3+2)*4"));
        assertEquals("6", calc.solveSum("2+(2*4)/2"));
        assertEquals("31", calc.solveSum("21+3*6/3+2*2/4-3*1+6"));
        assertEquals("28", calc.solveSum("21+18/3+4/4-3+6-3"));
        assertEquals("-6", calc.solveSum("6-2+3-2*4-5"));
        assertEquals("-113", calc.solveSum("(48/2)*(9+3)-400+3/-2"));
        assertEquals("-715", calc.solveSum("(((-2+3)-4)*5)+((-5+2)*100)-400"));
    }

    @Test
    public void testCalculatorWithDuplicatedPriority() {
        assertEquals("0", calc.solveSum("(2+1)-(2+1)"));
        assertEquals("5", calc.solveSum("2+(1+(1+1))"));
        assertEquals("200", calc.solveSum("4*5*10"));
        assertEquals("5", calc.solveSum("20/2/2"));
    }

    @Test
    public void testCalculatorWithFractions() {
        // note, fractions are rounded down to nearest int
        assertEquals("7", calc.solveSum("3.5+3.5"));
        assertEquals("1", calc.solveSum("10/6"));
        assertEquals("11", calc.solveSum("2.3*5"));
    }
}
