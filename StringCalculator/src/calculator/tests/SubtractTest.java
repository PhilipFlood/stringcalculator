/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.StringCalculator;

/**
 * @author eflophi
 *
 */
public class SubtractTest {

    StringCalculator calc = new StringCalculator();

    @Test
    public void testSubtract() {
        assertEquals("10", calc.subtract("20-10"));
        assertEquals("95", calc.subtract("100-5"));
        assertEquals("-25", calc.subtract("5-30"));
        assertEquals("11596", calc.subtract("12385-789"));
        assertEquals("-5", calc.subtract("-2-3"));
    }
}
