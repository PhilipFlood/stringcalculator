/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.StringCalculator;

/**
 * @author eflophi
 *
 */
public class multiplyTest {

    StringCalculator calc = new StringCalculator();

    @Test
    public void testMultiply() {
        assertEquals("50", calc.multiply("5*10"));
        assertEquals("-50", calc.multiply("-5*10"));
        assertEquals("100", calc.multiply("-5*-20"));
        assertEquals("4308486", calc.multiply("546*7891"));
    }
}
